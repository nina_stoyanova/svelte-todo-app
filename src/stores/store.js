// @ts-nocheck
import { writable } from "svelte/store";
import { derived } from "svelte/store";

export const darkTheme = writable(
  JSON.parse(localStorage.getItem("darkTheme")) || false
);
darkTheme.subscribe((value) => {
  return localStorage.setItem("darkTheme", JSON.stringify(value));
});

export const todos = writable(JSON.parse(localStorage.getItem("todos")) || []);
todos.subscribe((value) => {
  return localStorage.setItem("todos", JSON.stringify(value));
});

export const activeFilter = writable(null);

export const favoriteColors = writable(
  JSON.parse(localStorage.getItem("favorite-colors")) || []
);
favoriteColors.subscribe((value) => {
  return localStorage.setItem("favorite-colors", JSON.stringify(value));
});

export const uniqueColors = writable(
  JSON.parse(localStorage.getItem("unique-colors")) || []
);
uniqueColors.subscribe((value) => {
  return localStorage.setItem("unique-colors", JSON.stringify(value));
});

export const users = writable(JSON.parse(localStorage.getItem("users")) || []);
users.subscribe((value) => {
  return localStorage.setItem("users", JSON.stringify(value));
});

export const colors = writable(JSON.parse(localStorage.getItem("colors")) || []);
colors.subscribe((value)=>{
  return localStorage.setItem('colors', JSON.stringify(value))
})


export const filteredTodos = derived(
  [todos, activeFilter, favoriteColors],
  ([$todos, $activeFilter, $favoriteColors]) => {
    switch (true) {
      case "resolved":
        return $todos.filter((todo) => todo.completed === true);
      case "unresolved":
        return $todos.filter((todo) => todo.completed === false);
      case $activeFilter !== null && $activeFilter.startsWith("#"):
        return $todos.filter((todo) => {
          return todo.color === $activeFilter;
        });
      case "favorite colors":
        return $todos.filter((todo) => {
          return $favoriteColors.find((element) => {
            return todo.color === element;
          });
        });
      case $activeFilter !== null && /[A-Z]/.test($activeFilter):
        return $todos.filter((todo) => todo.username === $activeFilter);
      case "none":
        return $todos;
      default:
        return $todos;
    }
  }
);

