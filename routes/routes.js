// @ts-nocheck
import Home from "../src/routes/Home.svelte";
import DetailedTodo from "../src/routes/DetailedTodo.svelte";
import NotFound from "../src/routes/NotFound.svelte";

const routes = [
  { name: "/", component: Home },
  {
    name: "detailedTodo",
    component: DetailedTodo,
  },
  { name: "404", path: "404", component: NotFound },
];

export { routes };
3;


// import { navigateTo } from "svelte-router-spa";
// navigateTo("/detailedTodo");